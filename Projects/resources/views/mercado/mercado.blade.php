<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Sistema Compras-Ventas con Laravel y Vue Js- webtraining-it.com">
    <meta name="keyword" content="Sistema Compras-Ventas con Laravel y Vue Js">
    <title>Suma Aljasiñani</title>
    <!-- Icons -->
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/simple-line-icons.min.css')}}" rel="stylesheet">
    <!-- Main styles for this application -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet">

    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js">-->


</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
    <header class="app-header navbar">
        <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!--PONER LOGO-->
        <!--<a class="navbar-brand" href="#"></a>-->

        <ul class="nav navbar-nav d-md-down-none">
            <li class="nav-item px-3">
                <a class="nav-link" href="#">Suma Aljasiñani</a>
            </li>

        </ul>
        <ul class="nav-item ml-auto">
            <a type="button" class="btn btn-outline-success" href="{{ route('login') }}">Ingresar</a>
            <label for="" class="pl-2"></label>
            <a type="button" class="btn btn-outline-primary" href="">Registrar</a>

        </ul>
    </header>
    <br><br><br>
    <h1 class="text-success text-center">MERCADO SUMA ALJASIÑANI</h1>

    <div class="container-fluid">
        <!-- Estadísticas gráficos -->
        <div class="row">
            <div class="col-md-12">
                <!-- compras - meses -->
                <div class="card card-chart">
                    <div class="card-header">
                        <h4 class="text-center">Productos Disponibles</h4>
                    </div>
                    <div class="card-content">
                        <div class="ct-chart">
                            <table class="table table-bordered table-striped table-sm">
                                <thead>
                                    <tr class="bg-primary">
                                        <th>Nro.</th>
                                        <th>Imagen</th>
                                        <th>Producto</th>
                                        <th>Descripcion</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <a href="{{$num=1}}" disable></a>
                                    @foreach ($productos as $producto)
                                    <tr>
                                        <td>{{$num++}}</td>
                                        <td></td>
                                        <td>{{$producto->producto}}</td>
                                        <td></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--col-md-12-->
        </div>
        <!--row-->
        @push ('scripts')
        <script src="{{asset('js/Chart.min.js')}}"></script>
        @endpush

    </div>
    @if(Auth::check())
    @if (Auth::user()->idrol == 1)
    @include('plantilla.sidebaradministrador')
    @elseif (Auth::user()->idrol == 2)
    @include('plantilla.sidebarvendedor')
    @elseif (Auth::user()->idrol == 3)
    @include('plantilla.sidebarcomprador')
    @else

    @endif

    @endif

    <footer class="app-footer" id="mercado">
        <span class="ml-auto">Desarrollado por <a href="">Suma Aljasiñani</a></span>
    </footer>

    <!-- Bootstrap and necessary plugins -->
    <script src="{{asset('js/jquery.min.js')}}"></script>
    @stack('scripts')
    <script src="{{asset('js/popper.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/pace.min.js')}}"></script>
    <!-- Plugins and scripts required by all views -->
    <script src="{{asset('js/Chart.min.js')}}"></script>
    <!-- GenesisUI main scripts -->
    <script src="{{asset('js/template.js')}}"></script>
    <script src="{{asset('js/sweetalert2.all.min.js')}}"></script>
</body>

</html>